package org.bitbucket.rtempleton.workbench.dhisco.UltraDirect;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

public class PALSRQBean implements Serializable{

	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(PALSRQBean.class);
	
	private String GDA;
	private String GDI;
	private String CTY;
	private String CNT;
	private String CHN;
	private String PID;
	private String IND;
	private String OTD;
	private int NNT;
	private float MXR;
	private float MNR;
	private String RCU;
	private String NCU;
	private List<String> AMC;
	private int ROC;
	private int NPR;
	private int NRM;
	private int NBD;
	private String MTQ;
	
	

	public PALSRQBean(String segment) {
		this.AMC = new ArrayList<String>(6);
		parseSegment(segment, this);
	}
	
	static void parseSegment(String segment, PALSRQBean bean){
		String[] segments = segment.split("\\|");
		//skip the first segment since we know it's the type
		for (int i=1;i<segments.length;i++){
			
			switch(segments[i].substring(0, 3)){
				case "GDA":
					bean.GDA = getVal(segments[i]);
					break;
				case "GDI":
					bean.GDI = getVal(segments[i]);
					break;
				case "CTY":
					bean.CTY = getVal(segments[i]);
					break;
				case "CNT":
					bean.CNT = getVal(segments[i]);
					break;
				case "CHN":
					bean.CHN = getVal(segments[i]);
					break;
				case "PID":
					bean.PID = getVal(segments[i]);
					break;
				case "IND":
					bean.IND = getVal(segments[i]);
					break;
				case "OTD":
					bean.OTD = getVal(segments[i]);
					break;
				case "NNT":
					bean.NNT = Integer.parseInt(getVal(segments[i]));
					break;
				case "MXR":
					bean.MXR = Float.parseFloat(getVal(segments[i]));
					break;
				case "MNR":
					bean.MNR = Float.parseFloat(getVal(segments[i]));
					break;
				case "RCU":
					bean.RCU = getVal(segments[i]);
					break;
				case "NCU":
					bean.NCU = getVal(segments[i]);
					break;
				case "AMC":
					bean.AMC.add(getVal(segments[i]));
					break;
				case "ROC":
					bean.ROC = Integer.parseInt(getVal(segments[i]));
					break;
				case "NPR":
					bean.NPR = Integer.parseInt(getVal(segments[i]));
					break;
				case "NRM":
					bean.NRM = Integer.parseInt(getVal(segments[i]));
					break;
				case "NBD":
					bean.NBD = Integer.parseInt(getVal(segments[i]));
					break;
				case "MTQ":
					bean.MTQ = getVal(segments[i]);
					break;
				default:
					logger.warn("Unknown Segment:" + segments[i]);
			}
		}
	}
	
	static String getVal(String seg){
		return seg.substring(3, seg.length());
	}

	public String getGDA() {
		return GDA;
	}

	public void setGDA(String gDA) {
		GDA = gDA;
	}

	public String getGDI() {
		return GDI;
	}

	public void setGDI(String gDI) {
		GDI = gDI;
	}

	public String getCTY() {
		return CTY;
	}

	public void setCTY(String cTY) {
		CTY = cTY;
	}

	public String getCNT() {
		return CNT;
	}

	public void setCNT(String cNT) {
		CNT = cNT;
	}

	public String getCHN() {
		return CHN;
	}

	public void setCHN(String cHN) {
		CHN = cHN;
	}

	public String getPID() {
		return PID;
	}

	public void setPID(String pID) {
		PID = pID;
	}

	public String getIND() {
		return IND;
	}

	public void setIND(String iND) {
		IND = iND;
	}

	public String getOTD() {
		return OTD;
	}

	public void setOTD(String oTD) {
		OTD = oTD;
	}

	public int getNNT() {
		return NNT;
	}

	public void setNNT(int nNT) {
		NNT = nNT;
	}

	public float getMXR() {
		return MXR;
	}

	public void setMXR(int mXR) {
		MXR = mXR;
	}

	public float getMNR() {
		return MNR;
	}

	public void setMNR(int mNR) {
		MNR = mNR;
	}

	public String getRCU() {
		return RCU;
	}

	public void setRCU(String rCU) {
		RCU = rCU;
	}

	public String getNCU() {
		return NCU;
	}

	public void setNCU(String nCU) {
		NCU = nCU;
	}

	public List<String> getAMC() {
		return AMC;
	}

	public void setAMC(List<String> aMC) {
		AMC = aMC;
	}

	public int getROC() {
		return ROC;
	}

	public void setROC(int rOC) {
		ROC = rOC;
	}

	public int getNPR() {
		return NPR;
	}

	public void setNPR(int nPR) {
		NPR = nPR;
	}

	public int getNRM() {
		return NRM;
	}

	public void setNRM(int nRM) {
		NRM = nRM;
	}

	public int getNBD() {
		return NBD;
	}

	public void setNBD(int nBD) {
		NBD = nBD;
	}

	public String getMTQ() {
		return MTQ;
	}

	public void setMTQ(String mTQ) {
		MTQ = mTQ;
	}


}
