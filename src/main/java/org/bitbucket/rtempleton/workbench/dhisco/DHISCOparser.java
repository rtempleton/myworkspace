package org.bitbucket.rtempleton.workbench.dhisco;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

import org.bitbucket.rtempleton.workbench.dhisco.UltraDirect.UDTransactionBean;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * For the DHSIKO customer project which is parsing multi-record layout log data (req/resp) for travel service web site UltraDirect AMF Interface Specification
 * 
 * 
 * @author rtempleton
 *
 */
public class DHISCOparser {
	
	Gson gson = new GsonBuilder().create();

//	private String path = "/Users/rtempleton/Documents/workspace/MyWorkspace/src/test/resources/DHISCOsample.txt";
	private String path = "/Users/rtempleton/Projects/DHISCO/AALSRPtest.txt";

	public static void main(String[] args){
		new DHISCOparser().doit();
	}


	private void doit(){
		
		HashMap<String, Integer> list = new HashMap<String, Integer>();
		UDTransactionBean trans;

		try{
			InputStream fis=new FileInputStream(path);
			BufferedReader br=new BufferedReader(new InputStreamReader(fis));

			

			for (String line = br.readLine(); line != null; line = br.readLine()) {
				trans = new UDTransactionBean(line);
				if(trans.isInitialized())
					System.out.println(gson.toJson(trans));
				
//				String tmp[] = line.split("\\|\\|");
//				for (String t : tmp){
//					getSegType(t, list);
//					System.out.println(t);
//				}
			}

			br.close();
		}
		catch (Exception e){
			e.printStackTrace();
		}
		
		for(String ele : list.keySet())
			System.out.println(ele + " " + list.get(ele));
	}
	
	
	private void getSegType(String seg, HashMap<String, Integer> list){
		if(!(seg.startsWith("[") || seg.startsWith("]"))){
			String foo = seg.substring(0, seg.indexOf("|"));
			if(!list.containsKey(foo))
				list.put(foo, 1);
			else
				list.put(foo, (list.get(foo))+1);
		}
	}

}
