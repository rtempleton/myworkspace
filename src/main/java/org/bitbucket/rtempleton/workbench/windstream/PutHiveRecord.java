package org.bitbucket.rtempleton.workbench.windstream;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PutHiveRecord {

	Connection con;
	List<InfineraRecord> recs = new ArrayList<InfineraRecord>();
	final int threshold = 300;
	PreparedStatement stmt;

	public PutHiveRecord() {
		try {
			con = DriverManager.getConnection("jdbc:hive://edw2.field.hortonworks.com:10000/default");
			stmt = con.prepareStatement("insert into table infinera values (?,?,?,?,?,?)");
		}catch (Exception e) {
			e.printStackTrace(System.out);
			System.exit(-1);
		}
	}
	
	public void putRecord(InfineraRecord rec) {
		recs.add(rec);
		if(recs.size()== threshold) {
			flushCache();
			recs.clear();
		}
	}
	
	private void flushCache() {
		try {
			for(InfineraRecord rec : recs) {
				stmt.setObject(1, rec.getNodeId());
				stmt.setObject(2, rec.getSection());
				stmt.setObject(3, rec.getModule());
				stmt.setObject(4, rec.getTs());
				stmt.setObject(5, rec.getMeasure());
				stmt.setObject(6, rec.getVal());
				stmt.addBatch();
			}
			
			stmt.executeBatch();
		}catch(SQLException e) {
			e.printStackTrace(System.out);
		}
		
		recs.clear();
	}
}
