package org.bitbucket.rtempleton.workbench.windstream;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.sql.Timestamp;

import org.apache.hadoop.fs.Path;

import com.github.rtempleton.poncho.io.parsers.FloatParser;
import com.github.rtempleton.poncho.io.parsers.TimestampParser;


public class InfineraPivoter {


	String inpath = "/Users/rtempleton/Projects/Windstream/pm_data/";
//	Path outpath = new Path("/tmp/pm_data.orc");
	
	BufferedWriter bw = null;
	FileWriter fw = null;

	
//	PutHiveRecord putter = new PutHiveRecord();
	

	public static void main(String[] args) throws Exception {
		new InfineraPivoter();

	}
	
	public InfineraPivoter() throws Exception{
		File folder = new File(inpath);
		fw = new FileWriter("/Users/rtempleton/Projects/Windstream/pm_data.csv");
		bw = new BufferedWriter(fw);
		
		for (String file : folder.list()) {
			System.out.println(file);
			InfineraParser p = new InfineraParser(inpath + file);
			p.run();
		}
		
		bw.close();
	}
	
	
	class InfineraParser implements Runnable{
		
		private final String path;

		public InfineraParser(String path){
			this.path = path;
		}
		

		@Override
		public void run() {
			
			try {
				FileInputStream in = new FileInputStream(path);
				InputStreamReader inputStream = new InputStreamReader(in);
				BufferedReader br = new BufferedReader(inputStream);
				
				String nodeName = "", nodeId = "", section ="", module="", measure = "";
				TimestampParser parser = new TimestampParser("foo", "yyyy.MM.dd.HH.mm.ss", new Timestamp(1999, 1, 1, 0, 0, 0, 0));
				Timestamp ts;
				FloatParser fparser = new FloatParser("bar", null, null);
				Float val;
				
				boolean parsing = false;
				String[] header = new String[1];
				
				for (String line = br.readLine(); line != null; line = br.readLine()) {
					if(line.startsWith("NODEID")) {
						String[] s = line.split(",");
						s = s[1].split("@");
						nodeName = s[0];
						nodeId = s[1];
						continue;
					}else if(line.startsWith("h_") && !line.startsWith("h_CONTROL")) {
						header = line.split(",");
						section = header[0];
//						System.out.println("file: " + section);
						parsing=true;
						continue;
					}else if (parsing==true){
						String[] s = line.split(",");
						if(s.length==1)
							continue;
						module = s[0];
						ts=(Timestamp) parser.parse(s[2]);
						for(int i=5;i<s.length;i++) {
							measure = header[i];
							val = (Float) fparser.parse(s[i]);
							if(val==null)
								continue;
//							System.out.println(nodeName + ","+ nodeId+","+section+","+module+","+ts+","+measure+","+val);
							bw.write(nodeName + ","+ nodeId+","+section+","+module+","+ts+","+measure+","+val+"\n");
//							putter.putRecord(new InfineraRecord(nodeId, section, module, measure, ts, val));
						
						}
						
					}
					
					
				}
				br.close();
				inputStream.close();
				in.close();
				
			}catch (Exception e) {
				System.out.println(String.format("Error reading file: %s", path));
				e.printStackTrace();
				return;
			}
			
		}
		
	}
	
	

}
