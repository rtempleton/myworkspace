package org.bitbucket.rtempleton.workbench.windstream;

import java.sql.Timestamp;

public class InfineraRecord {

	private String nodeId, section, module, measure;
	private Timestamp ts;
	private Float val; 
	
	public InfineraRecord() {
	}
	
	public InfineraRecord(String nodeId, String section, String module, String measure, Timestamp ts, Float val) {
		this.nodeId = nodeId;
		this.section = section;
		this.module = module;
		this.measure = measure;
		this.ts = ts;
		this.val = val;
	}

	public String getNodeId() {
		return nodeId;
	}

	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getMeasure() {
		return measure;
	}

	public void setMeasure(String measure) {
		this.measure = measure;
	}

	public Timestamp getTs() {
		return ts;
	}

	public void setTs(Timestamp ts) {
		this.ts = ts;
	}

	public Float getVal() {
		return val;
	}

	public void setVal(Float val) {
		this.val = val;
	}
	

}
