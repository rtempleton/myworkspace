package org.bitbucket.rtempleton.workbench.hdfs;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

/**
 * File reader utility to search for a particular character within the first N bytes of a file in a provided file directory.
 * 
 * During an import of several hundred files into Hive, one or more files became corrupted and prevented Hive from running
 * all queries as it could not find the row delimiter. This resulted in an OOM error. I needed a utility to scan all the files
 * that had been imported to figure out which one was corrupted.
 * 
 * Standard out -> file name and position of the character being searched for. -1 if character is not present.
 * 
 * @author rtempleton
 *
 */
public class HdfsFileGulper {

	private FileSystem fs;
	private int threadCnt = 1;
	private int buffLen = 10000;
	private String searchString;

	public static void main(String[] args) throws Exception {
		Properties props = new Properties();
		try {
			props.load(new FileInputStream(args[0]));
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
		new HdfsFileGulper(props);


	}

	public HdfsFileGulper(Properties props) throws Exception{

		String dir = props.getProperty("searchDir.path");
		this.searchString = props.getProperty("searchString");

		try{
			this.threadCnt = Math.max(Integer.parseInt(props.getProperty("threadParallelism")), 1);
			System.out.println(String.format("Using %d threads", this.threadCnt));
		}catch (Exception e){
			this.threadCnt = 1;
			System.out.println("Error parsing threadCnt value. Defaulting to single thread");
		}

		try{
			this.buffLen = Integer.parseInt(props.getProperty("bufferLength"));
		}catch (Exception e){
			System.out.println("Error parsing bufferLength value. Defaulting to " + buffLen);
		}

		Configuration config = new Configuration();
		fs = FileSystem.get(config);
		FileStatus[] status = fs.listStatus(new Path(dir));



		Thread[] threads = new Thread[threadCnt];
		int counter = 0;
		while(counter<status.length){
			//skip objects that aren't files
			if(!status[counter].isFile()){
				counter++;
				continue;
			}
			for(int i=0;i<threads.length;i++){
				if(threads[i]==null || threads[i].getState().equals(Thread.State.TERMINATED)){
					threads[i] = new Thread(new Gulper(status[counter].getPath()));
					threads[i].start();
					counter++;
					break;
				}
			}

		}
	}




	class Gulper implements Runnable{

		private final Path path;

		public Gulper(Path path){
			this.path = path;
		}

		public void run() {
			InputStreamReader inputStream;
			BufferedReader br;
			try {
				inputStream = new InputStreamReader(fs.open(path));
				br = new BufferedReader(inputStream);
				char[] arry = new char[10000];
				br.read(arry);
				int res = String.copyValueOf(arry).indexOf(searchString);
				br.close();
				inputStream.close();
				System.out.println(path.getName() + " " + res);
			} catch (IOException e) {
				System.out.println(String.format("Error reading file: %s", path.getName()));
				e.printStackTrace();
				return;
			}

		}

	}

}
